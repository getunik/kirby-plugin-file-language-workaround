<?php

class CustomKirby extends Kirby
{
	public function branch()
	{
		$branch = count($this->options['languages']) > 0 ? 'multilang' : 'default';

		if ($branch === 'default') {
			return parent::branch();
		}

		return __DIR__ . DS . 'custom-multilang.php';
	}
}
