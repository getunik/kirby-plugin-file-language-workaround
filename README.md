# The Problem
The problem is described in Github issue [#575](https://github.com/getkirby/kirby/issues/575) 

# The Workaround
1. Add this plugin to your Kirby plugins - strictly speaking, you can add this code anywhere you like since it doesn't
really rely on the Kirby plugin functionality. You can add the code manually, or as a git submodule
```bash
git submodule add -b master git@bitbucket.org:getunik/kirby-plugin-file-language-workaround.git SOMEPATH/site/plugins/file-language-workaround
```

2. Add a `site.php` file to your Kirby web-root with the following contents:
```php
<?php

// load the main workaround entry point file
require(__DIR__ . DS . 'site' . DS . 'plugins' . DS . 'file-language-workaround' . DS . 'file-language-workaround.php');
// instantiate the customized Kirby object
$kirby = kirbyWorkaround(__DIR__);
```
