<?php

/**
 * Unmodified classes
 */
class Asset extends AssetAbstract {}
class Avatar extends AvatarAbstract {}
class Pages extends PagesAbstract {}
class Children extends ChildrenAbstract {}
class Files extends FilesAbstract {}
class Kirbytext extends KirbytextAbstract {}
class Kirbytag extends KirbytagAbstract {}
class Role extends RoleAbstract {}
class Roles extends RolesAbstract {}
class Users extends UsersAbstract {}
class User extends UserAbstract	{}

/**
 * Modified classes
 */
load([
	'content' => $BRANCHES_PATH . DS . 'multilang' . DS . 'content.php',
	'field' => $BRANCHES_PATH . DS . 'multilang' . DS . 'field.php',
	'file' => __DIR__ . DS . 'CustomFile.php',
	'language' => $BRANCHES_PATH . DS . 'multilang' . DS . 'language.php',
	'languages' => $BRANCHES_PATH . DS . 'multilang' . DS . 'languages.php',
	'page' => $BRANCHES_PATH . DS . 'multilang' . DS . 'page.php',
	'site' => $BRANCHES_PATH . DS . 'multilang' . DS . 'site.php',
]);
