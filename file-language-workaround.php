<?php

function kirbyWorkaround($kirbyRoot)
{
	$BRANCHES_PATH = implode(DS, [$kirbyRoot, 'kirby', 'branches']);
	require_once(__DIR__ . DS . 'CustomKirby.php');
	require_once(__DIR__ . DS . 'CustomFile.php');
	require_once(__DIR__ . DS . 'custom-multilang.php');

	return kirby(CustomKirby::class);
}
